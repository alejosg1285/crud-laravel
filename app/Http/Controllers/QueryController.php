<?php

namespace App\Http\Controllers;

use App\Departure;
use App\Employee;
use App\Position;
use Illuminate\Http\Request;

class QueryController extends Controller
{
    //
    public function allQuery(Request $request) {
        if (!$request->ajax()) return redirect('/');
        
        return [
            'departures' => Departure::with('positions')->get(),
            'positions' => Position::with('departure')->get(),
            'employee' => Employee::with('position')->get()
            ];
    }
}
