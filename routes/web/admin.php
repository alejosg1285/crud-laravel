<?php
    // Rutas de DepartureController
    Route::post('/departure/create', 'DepartureController@create')->name('departurecreate');
    
    Route::delete('/departure/delete/{id}', 'DepartureController@delete')->name('departuredelete');
    
    Route::put('/departure/update', 'DepartureController@update')->name('departureupdate');
    
    // Rutas de PositionController
    Route::post('/position/create', 'PositionController@create')->name('positioncreate');
    
    Route::delete('/position/delete/{id}', 'PositionController@delete')->name('positiondelete');
    
    Route::put('/position/update', 'PositionController@update')->name('positionupdate');
    
    // Rutas de EmployeeController
    Route::post('/employee/create','EmployeeController@create')->name('employeecreate');
    
    Route::delete('/employee/delete/{id}','EmployeeController@delete')->name('employeedelete');
    
    Route::put('/employee/update','EmployeeController@update')->name('employeeupdate');